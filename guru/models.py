from django.db import models

class Guru(models.Model):
    nama = models.CharField(max_length=50)
    nip = models.CharField(max_length=10)

    def __str__(self):
        return self.nama
