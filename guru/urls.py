from . import views
from django.urls import include, path

urlpatterns = [
    path('kelola/siswa/', include("siswa.urls"), name="siswa_list"),
    path('kelola/pengumuman/', include("pengumuman.urls"), name="pengumuman_list"),
    path('kelola/guru/', views.GuruList.as_view(), name='guru_list'),
    path('', views.landingGuru, name='guruLanding'),
    path('create/', views.GuruCreate.as_view(extra_context={'title': 'Tambah Guru'}), name='guru_new'),
    path('edit/<int:pk>', views.GuruUpdate.as_view(extra_context={'title': 'Edit Guru'}), name='guru_edit'),
    path('delete/<int:pk>', views.GuruDelete.as_view(), name='guru_delete'),
]
