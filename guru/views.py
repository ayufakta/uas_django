from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Guru

def landingGuru(request):
    return render (request, "landingGuru.html")

class GuruList(ListView):
    model = Guru

class GuruCreate(CreateView):
    model = Guru
    fields = ['nama', 'nip']
    success_url = reverse_lazy('guru_list')

class GuruUpdate(UpdateView):
    model = Guru
    fields = ['nama', 'nip']
    success_url = reverse_lazy('guru_list')

class GuruDelete(DeleteView):
    model = Guru
    success_url = reverse_lazy('guru_list')

def index(request):
    return HttpResponse("Hello, world")
