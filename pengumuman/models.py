from django.db import models
from siswa.models import Siswa
from guru.models import Guru

class Pengumuman(models.Model):
    penerima = models.ManyToManyField(Siswa)
    pengirim = models.ForeignKey(Guru, on_delete=models.CASCADE)
    isi = models.TextField()

    def __str__(self):
        return self.isi
