from . import views
from django.urls import include, path

urlpatterns = [
    path('', views.PengumumanList.as_view(), name="pengumuman_list"),
    path('create/', views.PengumumanCreate.as_view(extra_context={'title': 'Tambah Pengumuman'}), name='pengumuman_new'),
    path('edit/<int:pk>', views.PengumumanUpdate.as_view(extra_context={'title': 'Edit Pengumuman'}), name='pengumuman_edit'),
    path('delete/<int:pk>', views.PengumumanDelete.as_view(), name='pengumuman_delete'),
]
