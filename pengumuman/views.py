from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Pengumuman

class PengumumanList(ListView):
    model = Pengumuman

class PengumumanCreate(CreateView):
    model = Pengumuman
    fields = ['pengirim', 'penerima', 'isi']
    success_url = reverse_lazy('pengumuman_list')

class PengumumanUpdate(UpdateView):
    model = Pengumuman
    fields = ['pengirim', 'penerima', 'isi']
    success_url = reverse_lazy('pengumuman_list')

class PengumumanDelete(DeleteView):
    model = Pengumuman
    success_url = reverse_lazy('pengumuman_list')

def index(request):
    return HttpResponse("Hello, world")
