from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'welcome.html')

def indexSiswa(request):
    return render(request, 'landingSiswa.html')

def indexGuru(request):
    return render(request, 'landingGuru.html')
