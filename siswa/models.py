from django.db import models

class Siswa(models.Model):
    nama = models.CharField(max_length=200)
    nis = models.CharField(max_length=10)
    kelas = models.CharField(max_length=200)

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('siswa_edit', kwargs={'pk': self.pk})
