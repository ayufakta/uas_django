from . import views
from django.urls import include, path

urlpatterns = [
    path('landing/', views.landingSiswa, name='landingSiswa'),
    path('', views.SiswaList.as_view(), name='siswa_list'),
    path('cek/pengumuman', views.cekPengumuman, name='cek_pengumuman'),
    path('create/', views.SiswaCreate.as_view(extra_context={'title': 'Tambah Siswa'}), name='siswa_new'),
    path('edit/<int:pk>', views.SiswaUpdate.as_view(extra_context={'title': 'Edit Siswa'}), name='siswa_edit'),
    path('delete/<int:pk>', views.SiswaDelete.as_view(), name='siswa_delete'),
]
