from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Siswa
from pengumuman.models import Pengumuman

def landingSiswa(request):
    return render (request, "landingSiswa.html")

def cekPengumuman(request):
    if request.GET.get('nis'):
        dt_pengumuman = Pengumuman.objects.filter(penerima__nis=request.GET['nis'])
    else:
        message = 'You submitted nothing!'
    context = {'dt_pengumuman' : dt_pengumuman}
    return render (request, "hasilsearch.html", context)

class SiswaList(ListView):
    model = Siswa

class SiswaCreate(CreateView):
    model = Siswa
    fields = ['nama', 'nis']
    success_url = reverse_lazy('siswa_list')

class SiswaUpdate(UpdateView):
    model = Siswa
    fields = ['nama', 'nis']
    success_url = reverse_lazy('siswa_list')

class SiswaDelete(DeleteView):
    model = Siswa
    success_url = reverse_lazy('siswa_list')
